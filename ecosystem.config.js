module.exports = {
	apps: [
		{
			name: 'diplom',
			script: './index.js',
			// watch: true,
			// 'ignore_watch': ['node_modules', 'uploads'],
			env: {
				'PORT': 3001,
				'NODE_ENV': 'production',
				'SECRET': 'diplom'
			},
			'env_production': {
				'PORT': 3001,
				'NODE_ENV': 'production',
				'SECRET': 'diplom'
			}
		}
	]
}
