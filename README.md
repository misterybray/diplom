# Апишки

`BASE_URL - 34.217.100.201:3000`

## Авторизация

### `POST {BASE_URL}/api/auth/login` - Вход в систему

_Обязательные поля формы:_

`login` - имя пользователя

`password` - пароль

## Пользователи

### `POST {BASE_URL}/api/users` - Добавить нового сотрудника

_Обязательные поля формы:_

`fullname` - ФИО пользователя

`email` - почта

`password` - пароль

`department` - отдел

`position` - должность

_Необязательные поля формы:_

`phone` - номер телефона

`admin` - админ или нет (`boolean`)

`birthday` - день рождения (формат: дата)

---
### `GET {BASE_URL}/api/users?my={любой_текст}` - Список сотрудников

Если в запросе будет параметр `my`, то для админа покажутся его сотрудники

Если не будет, то по умолчанию все пользователи

---
### `PUT {BASE_URL}/api/users/:userId` - Редактировать данные сотрудника

`userId` - `_id` сотрудника, строка длиной в 24 символа

_Поля, возможные для редактирования:_

`fullname` - ФИО пользователя

`email` - почта

`password` - пароль

`department` - отдел

`position` - должность

`phone` - номер телефона

`admin` - админ или нет (`boolean`)

`birthday` - день рождения (формат: дата)

---
### `DELETE {BASE_URL}/api/users/:userId` - Удалить сотрудника

`userId` - `_id` сотрудника, строка длиной в 24 символа

## Задачи

### `POST {BASE_URL}/api/tasks` - Добавить новую задачу

_Обязательные поля формы:_

`title` - Тема задачи

`text` - Описание, детали задачи

`from` - от кого (нужно отправить `_id` сотрудника)

`to` - кому (нужно отправить `_id` сотрудника)

`deadline` - срок сдачи

_Необязательные поля формы:_

`comment` - Комментарий

---
### `GET {BASE_URL}/api/tasks` - Список задач

---
### `POST {BASE_URL}/api/tasks/:taskId` - Согласовать задачу

`taskId` - `_id` задачи, строка длиной в 24 символа

## Новости

### `GET {BASE_URL}/api/news` - Список новостей

## Клиенты

### `GET {BASE_URL}/api/clients` - Список клиентов

---
### `POST {BASE_URL}/api/clients` - Добавить клиента

_Обязательные поля формы:_

`fullname` - ФИО клиента

`processedBy` - Кем обработан (нужно отправить `_id` сотрудника)

`email` - Почта клиента

_Необязательные поля формы:_

`company` - Компания клиента

`phone` - Телефон клиента

## Статистика

### `POST {BASE_URL}/api/stats` - Вывести статистику

_Поля формы:_

__Один из полей должен быть обязательным__

`dateFrom` - Дата, начало промежутка

`dateTo` - Дата, конец промежутка
